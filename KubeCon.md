### Sections

### References and Other Resources
- [ArtifactHub.io](https://artifacthub.io/)

### Deploying Kubernetes in Classified Environments
- [Ali Monfre](https://www.linkedin.com/in/ali-monfre/)
- [Vlad Ungureanu](https://www.linkedin.com/in/vlad-ungureanu-b9aa3721b/)
- STIG compliance for:
  - Operating system
  - K8s cluster deployment
  - Thnigs that run in the cluster
- Solutions
  - OpenShift Platform
    - multi-node cluster deployments
    - RHCOS
    - Compliance operator
  - RKE2 (Rancher Government)
    - Edge/single node culster deployments
    - Runs on RHEL 8
- Security and Compliance
  - Naintain internally a golden container image that all software uses
  - Use OCI artifacts
- Cross domain solution
  - Automated one way data transfer
  - CDS from cloud providers
- Apollo Solution:
  - end-to-end management and automation

### Crossplane
- [marketplace.upbound.io](https://marketplace.upbound.io/)
- Deletion ordering problem
  - k8s is eventually consistent - good for create, bad for deletion
  - Deletion happens can result in orphaned managed resources
- crossplane v1.14 adds new `Usage` type
  - helps with complete cleanup
  - [Usage Documentation](https://docs.crossplane.io/v1.14/concepts/usages/)
- [Composition documentation](https://docs.crossplane.io/latest/concepts/compositions/)
  - Good but is missing several features
- [Composition Functions](https://docs.crossplane.io/latest/concepts/composition-functions/)
  - crossplane talks gRPC to each function
- Composite Resource (XR)
- CUE if functions:
  [CUE documentation](https://cuelang.org/docs/install/)
- [Crossplane Github repo](https://github.com/crossplane/crossplane)

### WASM
- [Cosmonic](https://cosmonic.com/)
- [wasmCloud](https://wasmcloud.com/)
- WASM  

### NATS
- [CNCF NATS](https://www.cncf.io/projects/nats/)
- [NATS Homepage](https://nats.io/)
- [WADM](https://wasmcloud.com/docs/ecosystem/wadm)

### Harbor
- [Harbor](https://goharbor.io/)
- OCI artifacts
- [Supports artifact signing](https://goharbor.io/docs/2.7.0/working-with-projects/working-with-images/sign-images/)
- [Software Build Of Materials (SBOM)](https://www.nginx.com/resources/glossary/software-bill-of-materials-sbom/)

### Cillium
- [Brief](./Briefs/Borkmann-Turning-Up-Performance-To-11.pdf)
- [IPVS walkthrough](https://kubernetes.io/blog/2018/07/09/ipvs-based-in-cluster-load-balancing-deep-dive/)
- [BBR blog post](https://isovalent.com/blog/post/accelerate-network-performance-with-cilium-bbr/)
- [BBR Demo Video](https://isovalent.com/videos/video-bbr-support-for-pods/)
- [BPF Host Routing](https://isovalent.com/blog/post/isovalent-enterprise-1-13/)
- [Cilium and eBPF](https://addozhang.medium.com/kubernetes-network-learning-with-cilium-and-ebpf-aafbf3163840)
- [Big TCP](https://isovalent.com/blog/post/big-tcp-on-cilium/)

### Cillium Contrib Fest
- Presenters:
  - [Joe Stringer](https://www.linkedin.com/in/joestringernz/)
  - [Bill Mulligan](https://www.linkedin.com/in/bamulligan/)
- [eBPF Video](https://www.youtube.com/watch?v=Wb_vD3XZYOA)
- [Brief](./Briefs/Cilium%20Overview%20for%20Developers%20-%20Kubecon%20NA%202023.pdf)