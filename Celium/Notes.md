<img src="../Images/Cilium_Logo.svg.png" width="200">

[Back](../README.md)

### Resources
- [Cilium Con](https://events.linuxfoundation.org/kubecon-cloudnativecon-north-america/co-located-events/ciliumcon/)
- [Cilium Documentation](https://cilium.io/)
- [Celium slack channel](https://cilium.herokuapp.com/)
- [Solo.io Academy](https://academy.solo.io/learn)
- [Learn K8s basics](https://kubernetes.io/docs/tutorials/kubernetes-basics/)

### Sections
- [Future Reading](#future-reading)
- [Adobe Solutions Brief](#adobe-solutions-brief)
- [Controlling Access to External APIs](#controlling-access-to-external-apis)
- [Advancing Cilium within the K8s Ecosystem](#advancing-cilium-within-the-k8s-ecosystem)
- [Cilium Mutual Authentication](#cilium-mutual-authentication)
- [Bridging gaps with BGP](#bridging-gaps-with-bgp)
- [Cilium CNI in ClickHouse Cloud](#cilium-cni-in-clickhouse-cloud)
- [Past, present, and future of Tetragon](#past-present-and-future-of-tetragon)

### Future Reading
- [How Ethos powers a cloud native transformation at adobe](https://blog.developer.adobe.com/how-ethos-powers-a-cloud-native-transformation-at-adobe-16c1a2e2f67a)
- [Cilium on AWS](https://isovalent.com/blog/post/2021-09-aws-eks-anywhere-chooses-cilium/)
- [Why BGP NGINX](https://www.nginx.com/blog/get-me-to-the-clusterwith-bgp/)
- [BGP Example Videos](https://isovalent.com/features/on-premises-integration/)
- [Cilium Service-mesh](https://cilium.io/use-cases/service-mesh/)
- [WireGuard](https://www.wireguard.com/)
- [Kyverno](https://kyverno.io/)


### Adobe Solutions Brief
- [Joseph Sandoval](https://www.linkedin.com/in/josephrsandoval/)
- [Tony Gosselin](https://www.linkedin.com/in/sfotony/)
- Ethos
  - Internal product
  - Infrastructure team developed
  - Started in 2017
  - Workloads
    - Single and multi-tenant
    - Cloud native and legacy
    - Untrusted workloads
    - Message pipelines, metrics, and other stateful workloads
    - Generative AI
  - Platform provides a lot of choices
  - Two Constants:
    - K8s
    - Cilium

[Top of page](#celium-notes)

### Controlling Access to External APIs
- Luis Ramirez
  - Engineer at [Super Orbital](https://www.linkedin.com/company/superorbital/)
- [Super Orbital Github](https://github.com/superorbital)
- [Brief Slide](../Briefs/Luis%20Ramirez%20--%20Controlling%20Access%20to%20External%20APIs%20with%20Cilium%20KubeCon%20+%20CloudNativeCon%20North%20America%202023.pdf)
- [Walkthrough from Brief](https://github.com/superorbital/ciliumcon-na-2023-l7-external-api-control)
- Cilium in K8s
  - eBPF allows for execution of code within the Linux kernel
  - Cilium agent runs on each agent pod
  - Network policies
    - All policies are loaded on pod
    - Policy are whitelisted by default
    - Rules split into ingress and egress
    - L3, L4, L7 policies
      - 3: basic connection
      - 4: ports and protoco
      - 7: applicaitons
        - ingress/ egress rules that are http aware
        - returns 403 back to pod on failure
      - fine grain control of api
  - Cilium capable of transparently injecting l4 proxy into any connection
  - L7 policies leverage the envoy proxy
  - Can be configured to intercept TLS encrypted connections
  
  [Top of page](#celium-notes)

### Advancing Cilium within the K8s Ecosystem
- [Idit Levine](https://www.linkedin.com/in/iditlevine/)
  - CEO and founder of [Solo.io](https://www.solo.io/)
- CAKES stack:
  - [Cilium](https://cilium.io/)
  - [Ambient Mesh](https://istio.io/latest/docs/ops/ambient/)
  - [Kubernetes](https://kubernetes.io/)
  - [Envoy Proxy](https://www.envoyproxy.io/)
  - [Spiffe](https://spiffe.io/)
- Gloo by Solo.io
  - [Gloo Edge](https://docs.solo.io/gloo-edge/latest/)
  - [Gloo Mesh]()
  - [Gloo Gateway]()

[Top of page](#celium-notes)

### Cilium Mutual Authentication
- [CFP: Completing Mutual Authentication + Stable Maturity](https://github.com/cilium/cilium/issues/28986)
- [Brief](../Briefs/CiliumCon%20Mutual%20Auth%20-%20Kubecon%20Chicago%202023.pdf)

### Bridging gaps with BGP
- [Marino Wijay](https://www.linkedin.com/in/mwijay/)
- [BGP control plane documentation](https://docs.cilium.io/en/latest/network/bgp-control-plane/)
- [Example lab](https://github.com/distributethe6ix/k8s_examples/tree/main/5-BGP-FRR-Cilium)
- [FRR Routing Documentation](https://www.packettracernetwork.com/ccna-ccnp-preparation/ccnp-frr.html)

[Top of page](#celium-notes)

### Cilium CNI in ClickHouse Cloud
- Timu Solodovnikov
  - Works at [ClickHouse](https://www.linkedin.com/company/clickhouseinc/)
- [Brief](../Briefs/ClickHouse_at_CiliumCon_US_2023.pptx)

[Top of page](#celium-notes)

### Past, present, and future of Tetragon
- [John Fastabend](https://www.linkedin.com/in/john-fastabend-27806212/)
- [Natalia Reka Ivanko](https://www.linkedin.com/in/natalia-ivanko/)
- [Tetragon Website](https://tetragon.io/)
- [Tetragon Github Page](https://github.com/cilium/tetragon)

[Top of page](#celium-notes)

### Secure Infrastructure with Combined Runtime and Network Security
- [Thomas Graf](https://www.linkedin.com/in/thomas-graf-73104547/)
- Network Policy with runtime context
  - All applicati0n containers in a pod have the potential to have the same level of access to other pds
- We want:
  - Container level security identity
- Cilium and Tetragon will help with this

[Top of page](#celium-notes)

### from Eventual to Strict Encryption - Securing Cilium's WireGuard Encryption
- [Leonard Cohnen](https://www.linkedin.com/in/leonard-cohnen-055518174/)
- [Brief](../Briefs/2023_CiliumCon_From_Eventual_to_Strict_Encryption.pdf)


[Top of page](#celium-notes)

### Come BGP With Me
- [Yutaro Hayakawa](https://www.linkedin.com/in/yutaro-hayakawa-33105115a/)
- [Daneyon Hansen](https://www.linkedin.com/in/daneyonhansen/)
- Cillium has a feature called "BGP Control Plane"
- BGP allows the pods to be directly accessed from outside the cluster

[Top of page](#celium-notes)

### Day 2 with Cilium - What to expect running at scale
- [Maxime Visonneau](https://www.linkedin.com/in/mvisonneau/)
- [Hemanth Malla](https://www.linkedin.com/in/hemanthmalla/)
- IPAM
  - [Cilium operator documentation](https://docs.cilium.io/en/stable/internals/cilium_operator/)
  - Cilium client lives inside Node
  - Look into surge allocation
- Deployments
  - Pre-flight checks
  - Use the [Install Documents](https://docs.cilium.io/en/stable/internals/cilium_operator/)
- Health and Observability
- EBPF and datapath
  - datapath with Cilium is very stable
  - Cluster IP + Graceful Termination
  - BPF map pressure
    - Does not cover all bpf maps
    - Limitation with LRU bpf maps 
- Addition knobs
- Takeaways

